# White Cloud Temple Programming Contest

### 介绍  
白云观生存竞赛，自第六轮起升级为五赛道：leetcode、面试题、flyai、论文、Python程序设计，完成任一赛道即可存活

- leetcode （剑指Offer75题：每日2题easy/1题medium/1题hard）+（官方[每日一题](https://leetcode-cn.com/circle/article/9EZfRE/)），有周赛或其他官方组织的活动时，将不布置剑指Offer的题目  
- 面试题 （5道笔试题，[范围](https://zhuanlan.zhihu.com/p/77014561)）+ （1道面试大题，[范围](https://www.julyedu.com/question/topic_list/23)）  
- flyai （每轮随机一道cv 一道nlp）  
- 论文 （每轮一篇论文，参赛者可自己点论文，每周需按照阅读进度打卡）
- Python程序设计 (分为多个小赛道：NumPy、Pandas、PyTorch等等，每日至少完成一项即可)

题目掷骰子决定，每日24点前打卡，未打卡踢出群聊，每周日开放进群通道。

### 相关资料
- [算法导论（第三版）上](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E7%AE%97%E6%B3%95%E5%AF%BC%E8%AE%BA%EF%BC%88%E7%AC%AC%E4%B8%89%E7%89%88%EF%BC%89%E4%B8%8A.pdf)
- [算法导论（第三版）下](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E7%AE%97%E6%B3%95%E5%AF%BC%E8%AE%BA%EF%BC%88%E7%AC%AC%E4%B8%89%E7%89%88%EF%BC%89%E4%B8%8B.pdf)
- [算法（第四版）上](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E7%AE%97%E6%B3%95%EF%BC%88%E7%AC%AC4%E7%89%88%EF%BC%89%E4%B8%8A.pdf)
- [算法（第四版）下](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E7%AE%97%E6%B3%95%EF%BC%88%E7%AC%AC4%E7%89%88%EF%BC%89%E4%B8%8B.pdf)
- [MIT 6.006: Introduction to Algorithms (2008)](https://www.bilibili.com/video/av94300528/)
- [算法神探：一部谷歌首席工程师写的CS小说](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E7%AE%97%E6%B3%95%E7%A5%9E%E6%8E%A2%EF%BC%9A%E4%B8%80%E9%83%A8%E8%B0%B7%E6%AD%8C%E9%A6%96%E5%B8%AD%E5%B7%A5%E7%A8%8B%E5%B8%88%E5%86%99%E7%9A%84CS%E5%B0%8F%E8%AF%B4.pdf)
- [剑指OFFER 名企面试官精讲典型编程题 第2版](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E5%89%91%E6%8C%87OFFER%20%20%E5%90%8D%E4%BC%81%E9%9D%A2%E8%AF%95%E5%AE%98%E7%B2%BE%E8%AE%B2%E5%85%B8%E5%9E%8B%E7%BC%96%E7%A8%8B%E9%A2%98%20%20%E7%AC%AC2%E7%89%88.pdf)
- [七月在线 名企AI面经100篇](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E5%90%8D%E4%BC%81AI%E9%9D%A2%E7%BB%8F100%E7%AF%87.pdf)
- [七月在线 名企AI面试100题](https://gitee.com/white-cloud-temple/wctpc/raw/master/material/%E5%90%8D%E4%BC%81AI%E9%9D%A2%E8%AF%95100%E9%A2%98.pdf)

### 题目汇总
- Leetcode:
    - [剑指Offer](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/index-lcof.md)

### 第七轮
时间：2020年03月30日-2020年04月05日
- Leetcode:
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-62-yuan-quan-zhong-zui-hou-sheng-xia-de-shu-zi.md)_ [03月30日 每日一题 面试题62. 圆圈中最后剩下的数字](https://leetcode-cn.com/problems/yuan-quan-zhong-zui-hou-sheng-xia-de-shu-zi-lcof/)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-26-shu-de-zi-jie-gou.md)_ [03月30日 剑指Offer 面试题26 树的子结构](https://leetcode-cn.com/problems/shu-de-zi-jie-gou-lcof)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/0912-sort-an-array.md)_ [03月31日 每日一题 912. 排序数组](https://leetcode-cn.com/problems/sort-an-array/)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-28-dui-cheng-de-er-cha-shu.md)_ [03月31日 剑指Offer 面试题28 对称的二叉树](https://leetcode-cn.com/problems/dui-cheng-de-er-cha-shu-lcof)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-29-shun-shi-zhen-da-yin-ju-zhen.md)_ [03月31日 剑指Offer 面试题29 顺时针打印矩阵](https://leetcode-cn.com/problems/shun-shi-zhen-da-yin-ju-zhen-lcof)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/1111-maximum-nesting-depth-of-two-valid-parentheses-strings.md)_ [04月01日 每日一题 1111 有效括号的嵌套深度](https://leetcode-cn.com/problems/maximum-nesting-depth-of-two-valid-parentheses-strings/)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-30-bao-han-minhan-shu-de-zhan.md)_ [04月01日 剑指Offer 面试题30 包含min函数的栈](https://leetcode-cn.com/problems/bao-han-minhan-shu-de-zhan-lcof)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-32-2-cong-shang-dao-xia-da-yin-er-cha-shu-ii.md)_ [04月01日 剑指Offer 面试题32 - II 从上到下打印二叉树 II](https://leetcode-cn.com/problems/cong-shang-dao-xia-da-yin-er-cha-shu-ii-lcof)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/0289-game-of-life.md)_ [04月02日 每日一题 289. 生命游戏](https://leetcode-cn.com/problems/game-of-life/)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-31-zhan-de-ya-ru-dan-chu-xu-lie.md)_ [04月02日 剑指Offer 面试题31 栈的压入、弹出序列](https://leetcode-cn.com/problems/zhan-de-ya-ru-dan-chu-xu-lie-lcof)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/0008-string-to-integer-atoi.md)_ [04月03日 每日一题 8. 字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi/)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/lcof-32-1-cong-shang-dao-xia-da-yin-er-cha-shu.md)_ [04月03日 剑指Offer 面试题32 - I 从上到下打印二叉树](https://leetcode-cn.com/problems/cong-shang-dao-xia-da-yin-er-cha-shu-lcof)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/)_ [04月04日 每日一题 ]()
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/biweekly-0023.md)_ [04月04日 第  23 场双周赛](https://leetcode-cn.com/contest/biweekly-contest-23)
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/)_ [04月05日 每日一题 ]()
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/leetcode/weekly-0183.md)_ [04月05日 第 183 场周赛](https://leetcode-cn.com/contest/weekly-contest-183)
- 面试题:
    - _[十道笔试题](https://gitee.com/white-cloud-temple/wctpc/blob/master/document_questions/0002.md)_
    - _[两道面试大题](https://gitee.com/white-cloud-temple/wctpc/blob/master/interview_questions/0002.md)_
- FlyAI：（二选一）
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/flyai/0019-ner-new.md)_ [19 中文的命名实体识别](https://www.flyai.com/d/NER_new) 
    - _[点击切磋打卡](https://gitee.com/white-cloud-temple/wctpc/blob/master/flyai/0008-captcha.md)_ [08 常见物体识别](https://www.flyai.com/d/Captcha)
- 论文：（四选一）
    - **[ImageNet Classiﬁcation with Deep Convolutional Neural Networks](https://gitee.com/white-cloud-temple/papers/blob/master/002-imagenet-classification-with-deep-convolutional-neural-networks/002-imagenet-classification-with-deep-convolutional-neural-networks.md)** Alex Krizhevsky, I Sutskever, G Hinton. ImageNet Classification with Deep Convolutional Neural Networks[C]// International Conference on Neural Information Processing Systems. Curran Associates Inc. 2012.
    - **[A Neural Probabilistic Language Model](https://gitee.com/white-cloud-temple/papers/blob/master/003-a-neural-probabilistic-language-model/003-a-neural-probabilistic-language-model.md)** Bengio Y, Ducharme R, Vincent P, et al. A neural probabilistic language model[J]. Journal of Machine Learning Research, 2003, 3(6): 1137-1155.
    - **[U-Net: Convolutional Networks for Biomedical Image Segmentation](https://gitee.com/white-cloud-temple/papers/blob/master/006-u-net-convolutional-networks-for-biomedical-image-segmentation/006-u-net-convolutional-networks-for-biomedical-image-segmentation.md)**  Ronneberger O , Fischer P , Brox T . U-Net: Convolutional Networks for Biomedical Image Segmentation[J]. 2015.
    - **[Scalable trust-region method for deep reinforcement learning using Kronecker-factored approximation](https://gitee.com/white-cloud-temple/papers/blob/master/007-scalable-trust-region%20method-for-deep-reinforcement-learning-using-kronecker-factored%20approximation/007-scalable-trust-region%20method-for-deep-reinforcement-learning-using-kronecker-factored%20approximation.md)** Wu Y, Mansimov E, Liao S, et al. Scalable trust-region method for deep reinforcement learning using Kronecker-factored approximation[J]. arXiv: Learning, 2017.
    - **[Optimizing Neural Networks with Kronecker-factored Approximate Curvature](https://gitee.com/white-cloud-temple/papers/blob/master/008-optimizing-neural-networks-with-kronecker-factored-approximate-curvature/008-optimizing-neural-networks-with-kronecker-factored-approximate-curvature.md)** Martens J, Grosse R. Optimizing Neural Networks with Kronecker-factored Approximate Curvature[J]. arXiv: Learning, 2015.
         
- Python程序设计
    |         |                             0330                             |                             0331                             |                             0401                             |                             0402                             |                             0403                             |                             0404                             |                             0405                             |
    | :-----: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
    |  NumPy  | [001](https://gitee.com/white-cloud-temple/pp/blob/master/numpy/001.md) | [002](https://gitee.com/white-cloud-temple/pp/blob/master/numpy/002.md) | [003](https://gitee.com/white-cloud-temple/pp/blob/master/numpy/003.md) | [004](https://gitee.com/white-cloud-temple/pp/blob/master/numpy/004.md) | [005](https://gitee.com/white-cloud-temple/pp/blob/master/numpy/005.md) | [006](https://gitee.com/white-cloud-temple/pp/blob/master/numpy/006.md) | [007](https://gitee.com/white-cloud-temple/pp/blob/master/numpy/007.md) |
    | Pandas  | [001](https://gitee.com/white-cloud-temple/pp/blob/master/pandas/001.md) | [002](https://gitee.com/white-cloud-temple/pp/blob/master/pandas/002.md) | [003](https://gitee.com/white-cloud-temple/pp/blob/master/pandas/003.md) | [004](https://gitee.com/white-cloud-temple/pp/blob/master/pandas/004.md) | [005](https://gitee.com/white-cloud-temple/pp/blob/master/pandas/005.md) | [006](https://gitee.com/white-cloud-temple/pp/blob/master/pandas/006.md) | [007](https://gitee.com/white-cloud-temple/pp/blob/master/pandas/007.md) |
    | PyTorch | [001](https://gitee.com/white-cloud-temple/pp/blob/master/torch/001.md) | [002](https://gitee.com/white-cloud-temple/pp/blob/master/torch/002.md) | [003](https://gitee.com/white-cloud-temple/pp/blob/master/torch/003.md) | [004](https://gitee.com/white-cloud-temple/pp/blob/master/torch/004.md) | [005](https://gitee.com/white-cloud-temple/pp/blob/master/torch/005.md) | [006](https://gitee.com/white-cloud-temple/pp/blob/master/torch/006.md) | [007](https://gitee.com/white-cloud-temple/pp/blob/master/torch/007.md) |
    | TensorFlow | [001](https://gitee.com/white-cloud-temple/pp/blob/master/tensorflow/001.md) | [002](https://gitee.com/white-cloud-temple/pp/blob/master/tensorflow/002.md) | [003](https://gitee.com/white-cloud-temple/pp/blob/master/tensorflow/003.md) | [004](https://gitee.com/white-cloud-temple/pp/blob/master/tensorflow/004.md) | [005](https://gitee.com/white-cloud-temple/pp/blob/master/tensorflow/005.md) | [006](https://gitee.com/white-cloud-temple/pp/blob/master/tensorflow/006.md) | [007](https://gitee.com/white-cloud-temple/pp/blob/master/tensorflow/007.md) |

### 往届（自第六轮起升级为五赛道）
1. [第一轮 2020年02月28日-2020年03月01日](contest/001.md)
2. [第二轮 2020年03月02日-2020年03月04日](contest/002.md)
3. [第三轮 2020年03月06日-2020年03月08日](contest/003.md)
4. [第四轮 2020年03月11日-2020年03月15日](contest/004.md)
5. [第五轮 2020年03月16日-2020年03月22日](contest/005.md)
6. [第六轮 2020年03月23日-2020年03月29日](contest/006.md)
