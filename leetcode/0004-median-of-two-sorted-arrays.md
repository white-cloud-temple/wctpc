# 4. Median of Two Sorted Arrays
来源：[力扣（LeetCode）](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)

### Description
There are two sorted arrays **nums1** and **nums2** of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

You may assume **nums1** and **nums2** cannot be both empty.

**Example 1:**

```
nums1 = [1, 3]  
nums2 = [2]

The median is 2.0
```

**Example 2:**

```
nums1 = [1, 2]  
nums2 = [3, 4]  
  
The median is (2 + 3)/2 = 2.5
```

### 打卡模版

```
ID：

提交记录：  
|提交时间|提交结果|执行用时|内存消耗|语言|  
|---|---|---|---|---|
||||||  

代码：

```