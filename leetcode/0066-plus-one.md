# 66. Plus One
来源：[力扣（LeetCode）](https://leetcode-cn.com/problems/plus-one)

### Description
Given a **non-empty** array of digits representing a non-negative integer, plus one to the integer.

The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.

You may assume the integer does not contain any leading zero, except the number 0 itself.

**Example 1:**

```
Input: [1,2,3]  
Output: [1,2,4]  
Explanation: The array represents the integer 123.  
```

**Example 2:**

```
Input: [4,3,2,1]  
Output: [4,3,2,2]  
Explanation: The array represents the integer 4321.
```

### 打卡模版
```
ID：

提交记录：  
|提交时间|提交结果|执行用时|内存消耗|语言|  
|---|---|---|---|---|
||||||  

代码：

```

