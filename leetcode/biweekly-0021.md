# 第 21 场双周赛
来源：[力扣（LeetCode）](https://leetcode-cn.com/contest/biweekly-contest-21)

### 题目列表
- 上升下降字符串 
- 每个元音包含偶数次的最长子字符串  
- 二叉树中的最长交错路径  
- 二叉搜索子树的最大键值和

### 5336. 上升下降字符串
来源：[力扣（LeetCode）](https://leetcode-cn.com/problems/increasing-decreasing-string/)

### 5337. 每个元音包含偶数次的最长子字符串
来源：[力扣（LeetCode）](https://leetcode-cn.com/problems/find-the-longest-substring-containing-vowels-in-even-counts/)

### 5338. 二叉树中的最长交错路径
来源：[力扣（LeetCode）](https://leetcode-cn.com/problems/longest-zigzag-path-in-a-binary-tree/)

### 5339. 二叉搜索子树的最大键值和
来源：[力扣（LeetCode）](https://leetcode-cn.com/problems/maximum-sum-bst-in-binary-tree/)

### 打卡模版

```
ID：

提交记录：  
|排名|用户名|得分|完成时间|题目1 (4)|题目2 (4)|题目3 (5)|题目4 (6)|  
|---|---|---|---|---|---|---|---|
||||||||| 

1. 上升下降字符串 

    代码：

2. 每个元音包含偶数次的最长子字符串  

    代码：

3. 二叉树中的最长交错路径  

    代码：

4. 二叉搜索子树的最大键值和  

    代码：
```