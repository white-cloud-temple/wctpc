# 485. Max Consecutive Ones
来源：[力扣（LeetCode）](https://leetcode-cn.com/problems/max-consecutive-ones)

### Description
Given a binary array, find the maximum number of consecutive 1s in this array.

**Example 1:**
```
Input: [1,1,0,1,1,1]  
Output: 3  
Explanation: The first two digits or the last three digits are consecutive 1s. 
             The maximum number of consecutive 1s is 3.  
```

**Note:**
1. The input array will only contain `0` and `1`.
2. The length of input array is a positive integer and will not exceed 10,000

### 打卡模版

```
ID：

提交记录：  
|提交时间|提交结果|执行用时|内存消耗|语言|  
|---|---|---|---|---|
||||||  

代码：

```

